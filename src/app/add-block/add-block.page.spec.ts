import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddBlockPage } from './add-block.page';

describe('AddBlockPage', () => {
  let component: AddBlockPage;
  let fixture: ComponentFixture<AddBlockPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBlockPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddBlockPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
