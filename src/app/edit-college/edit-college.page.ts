import { Component, OnInit } from '@angular/core';
import { CollegeService } from '../college.service';
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormBuilder } from "@angular/forms";
@Component({
  selector: 'app-edit-college',
  templateUrl: './edit-college.page.html',
  styleUrls: ['./edit-college.page.scss'],
})
export class EditCollegePage implements OnInit {
  updateCollegeForm: FormGroup;
  id: any;
  constructor(private CollegeService: CollegeService,
    private actRoute: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder) { this.id = this.actRoute.snapshot.paramMap.get('id');}

  ngOnInit() {
    this.getCollegeData(this.id);
    this.updateCollegeForm = this.fb.group({
      name: [''],
      address: ['']
    })
  }

  getCollegeData(id) {
    this.CollegeService.getCollegeId(id).subscribe(res => {
      this.updateCollegeForm.setValue({
        name: res['name'],
        address: res['address']
      });
    });
  }

  updateForm() {
    if (!this.updateCollegeForm.valid) {
      return false;
    } else {
      this.CollegeService.updateCollege(this.id, this.updateCollegeForm.value)
        .subscribe((res) => {
          console.log(res)
          this.updateCollegeForm.reset();
          this.router.navigate(['../college']);
        })
    }
  }



}
