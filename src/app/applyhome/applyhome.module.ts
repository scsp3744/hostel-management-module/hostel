import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApplyhomePageRoutingModule } from './applyhome-routing.module';

import { ApplyhomePage } from './applyhome.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApplyhomePageRoutingModule
  ],
  declarations: [ApplyhomePage]
})
export class ApplyhomePageModule {}
