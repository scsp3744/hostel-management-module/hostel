import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CollegeService {

  constructor(public http: HttpClient) { }

  getCollegeId(id){
    return this.http.get('http://localhost:3000/college/' + id);
  }

  getCollege(){
    return this.http.get('http://localhost:3000/college');
  }

  createCollege(college){
    console.log(college)
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.post('http://localhost:3000/college', college, {headers: headers}).toPromise()
  }

  updateCollege(id, college){
    console.log(college)
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.put('http://localhost:3000/college/' + id, college, {headers: headers})
  }

  deleteCollege(id){
    this.http.delete('http://localhost:3000/college/' + id).subscribe((res:any) =>
    {console.log(res);
  });
}
}
