import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { BlockService } from '../block.service';
import { AddBlockPage } from '../add-block/add-block.page';

@Component({
  selector: 'app-block',
  templateUrl: './block.page.html',
  styleUrls: ['./block.page.scss'],
})
export class BlockPage implements OnInit {
  blocks: any;
  constructor(public nav: NavController, 
    public BlockService: BlockService, 
    public modalCtrl: ModalController) {
   
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.blocks =[];
    this.BlockService.getBlock().subscribe(blocks =>{
    this.blocks = blocks;
  })
  }

  async addBlock(){
    let modal = await this.modalCtrl.create({component:AddBlockPage});

    modal.onDidDismiss().then(block => {
      block = block.data;
      if(block){
        this.BlockService.createBlock(block).then((block) => {
          this.blocks.push(block);
          const lastIndex = this.blocks.length-1;
        })
      }
    });
     modal.present();
  }

  deleteBlock(block){
    let index = this.blocks.indexOf(block);

    if(index >-1){
      this.blocks.splice(index, 1);
    }
    this.BlockService.deleteBlock(block._id);
  }

}
